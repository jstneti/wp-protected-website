<?php
/**
 * @package wp_protected_website
 * @version 0.1
 */
/*
Plugin Name: Protected website
Plugin URI: http://www.rakarweb.com
Description: Allow access to whole website only to signed in users.
Author: Valentina Rakar at Rakar Web
Version: 0.1
Author URI: http://www.rakarweb.com
*/

/**
 * Redirect non singed in users to wp-login.php pahe.
 */
function my_page_template_redirect() {
	if ( ! is_user_logged_in() ) {
		wp_redirect( 'wp-login.php' );
		exit;
	}
}
add_action( 'template_redirect', 'my_page_template_redirect' );
?>
